# Clasificación de buques militares, "Few-Shot Learning vs. CNN"

El conocimiento de las flotas enemigas para la preparación de misiones militares puede suponer una enorme ventaja, pero la falta de información mediante sistemas como AIS de los buques militares, hacen pensar en alternativas como el uso de imágenes satelitales para llevar a cabo la clasificación de estos buques. Sin embargo, los conjuntos de datos etiquetados son muy limitados, por lo que se realizará un estudio comparativo de modelos clasificadores a partir de imágenes satelitales partiendo de un dataset con pocas muestras, mediante la implementación de dos modelos, uno de redes convolucionales y otros utilizando técnicas de Few-Shot Learning. 

- [Clasificación de buques militares, "Few-Shot Learning vs. CNN"](#clasificación-de-buques-militares-few-shot-learning-vs-cnn)
  - [Organización del respositorio](#organización-del-respositorio)
    - [Transfer Learning, Fine Tuning.](#transfer-learning-fine-tuning)
    - [Notebooks de pruebas](#notebooks-de-pruebas)
    - [Código del trabajo](#código-del-trabajo)
    - [Resultados](#resultados)

## Organización del respositorio

Este respositorio recoge los scripts de código y resultados de la comparativa realizada en el Trabajo Fin de Estudios (TFE).

### Transfer Learning, Fine Tuning.

La carpeta de `tranfer-learning_ft` contiene los resultados de realizar el proceso de transfer learning Fine Tuning para adaptar el modelo de EfficientNet-B5 con pesos de ImageNet a un modelo con los pesos adecuados para obtener vectores de características de los buques utilizando los conjuntos de clases definidos en la memoria del TFE.

* `efficientnetb5_ft_summary.txt`: Contiene al descripción por capas del modelo de efficientnet-b5 con las capas fully connected preparadas para las 9 clases con las que se ha realizado el Fine Tuning.
* `confusion_matrix_ft.png`: Imagen de la matriz de confusión obtenida tras el proceso de transfer learning.
* `loss_accuracy_ft.png`: Imagen de los valores de *loss* y *accuracy* obtenidos durante el proceos de entrenamiento.

![Matriz de confusión de FT](./transfer-learning_ft/confusion_matrix_ft.png)
![Valores de loss y accuracy de FT](./transfer-learning_ft/loss_accuracy_ft.JPG)

### Notebooks de pruebas

La carpeta de `notebooks` contiene el notebook utilizado para las pruebas y muestra de resultados del TFE.

### Código del trabajo

En el directorio `src` se incluyen los scripts de python utilizados durante los entrenamientos necesarios para realizar la comparativa del trabajo.

### Resultados

El directorio `results` se incluyen todos los resultados obtenidos para la comparativa. Entre ellos se encuentran las matrices de confusión, los valores de métricas como *recall*, *precision*, *f-1*, *accuracy* y *tiempo de entrenamiento* de las diferentes iteraciones repetitivas que se han realizado, además de las gráficas resultantes de la representación de los valores recogidos.

![Gráficas comparativa de las métricas](results/graph_metrics.png)

* **imgs**: Contiene todas las imágenes de las matrices de confusión de los diferentes entrenamientos. Al igual que en *reports*, se divide en las subcarpetas `results/imgs/cnn` y `results/imgs/fsl` y dentro de cada una de ellas se encuentra una carpeta por cada conjunto de pruebas `1-shot_1-novel_1-extra`, `1-shot_5-novel_2-extra`, `5-shot_1-novel_1-extra`, `5-shot_5-novel_2-extra` que contienen las imágenes de las matrices de confusión de cada iteración dentro del conjunto.

* **reports**: Los valores de las métricas de *recall*, *precision*, *f-1*, *accuracy* y *tiempo de entrenamiento*, se adjuntan en ficheros CSV. Se encuentran repartidos entre las subcarpetas `results/reports/cnn` y `results/reports/fsl`. El nombre de los ficheros CSV indica a qué conjunto de pruebas pertenece y la fecha y hora en la que se obtuvo el informe (`1-shot_1-novel_1-extra_20210905-061403`, 1 img de referencia, con 1 clase del grupo novel y 1 clase del grupo extra).
Las cabeceras de los ficheros CSV que recogen los datos, se organizan según el siguiente esquema:

    * **classes**: Es una lista de listas de python para indicar el nombre de las clases utilizadas en el entrenamiento.

        ```python
        # Lista principal
        [<clases_base>, <clases_novel>, <clases_extra>]

        # Sublistas <clases_base>, <clases_novel>, <clases_extra>
        ['Arleigh_Burke_class_destroyers', 'Command_ship_A', ...]

        # Ejemplo completo
        classes = [['Arleigh_Burke_class_destroyers', 'Command_ship_A', ...], 
                  ['Medical_ship', 'Container_ship', ...],
                  ['Destroyer_Murasame', 'Minesweeper_Segura']]

        # Acceso a los elementos
        # Primera de las clases base
        print(classes[0][0])  # Arleigh_Burke_class_destroyers
        # Primera de las clases novel
        print(classes[1][0])  # Medical_ship
        # Primera de las clases extra
        print(classes[2][0])  # Destroyer_Murasame
        ```

    * **trainning_duration**: Es un valor Float que indica la duración del tiempo de entrenamiento.
    * **metrics_per_class**: Diccionario de python en el que las claves son los nombres de las clases de buques y los valores un listado de las métricas de cada clase recogidas, en el que:
        * *1a posición*: Contiene el valor de recalling
        * *2a posición*: Contiene el valor de precision
        * *3a posición*: Contiene el valor de f-1
        * *4a posicion*: Contiene el valor de accuracy

      ```python
      {
          'Arleigh_Burke_class_destroyers': [0.5080645161290323, 0.984375, 0.6702127659574468, 0.9162162162162162],
          ...
      }
      # El buque tipo 'Arleigh_Burke_class_destroyers' tiene un recall de 0.508, una precision de 0.98, un valor de f-1 de 0.67 y un valor de accuracy de 0.92
      ```

    * **confusion_matrix_path**: Es un valor String que indica el path dentro del sistema de lanzado de los scripts, donde se encuentra la matriz de confusión correspondiente a dicha prueba. Si nos fijamos en un ejemplo, en la parte final de path, vemos como:
        * Pertenece a una prueba de Few-Shot Learning por `fsl`.
        * Pertenece al conjunto de pruebas de 1 sola imagen de referencia, 5 clases novel y 2 clases extra por `1-shot_5-novel_2-extra`.
        * Se realizó el 04 de sept. del 2021 a las 18:01:19 por `20210904-180119_0.png`

        ```bash
        fsl/1-shot_5-novel_2-extra/20210904-180119_0.png
        ```