import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import tensorflow as tf
from tensorflow.keras import layers
import utils.factory as factory
from utils.config import Config

from sklearn.neighbors import NearestCentroid

# mc
from sklearn.metrics import confusion_matrix
import seaborn as sn
import pandas as pd
import scipy.ndimage as ndimage


from glob import glob
import os
import imageio
import random
import math
import time
import shutil
import cv2
import argparse

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   
os.environ["CUDA_VISIBLE_DEVICES"]="0"

# adjust the TF verbosity by changing the value of TF_CPP_MIN_LOG_LEVEL:
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def get_metrics(y_true, y_pred, classes, cm_path=None):
    """Función para representar la matriz de confusión y obtener las 
       métricas por clase
       
       Args:
           y_true: [List]. Clases verdaders
           y_pred: [List]. Clases predichas
           classes: [List]. Listado de nombre de las clases
           cm_path: [String]. Si se aporta un path se guardará la img de la
                              matriz de confusión ahí.
           
       Returns:
           metrics: [Dict]. Clave el nombre de la clase, valor el listado con
                            las métricas por clase [recall, precision, f1, accuracy]
    """
    # Imprimimos la matriz de confusión
    cm = confusion_matrix(y_true, y_pred)

    df_cm = pd.DataFrame(cm, index = [n for n in classes],
                      columns = [n for n in classes])
    plt.figure(figsize = (16,12))
    sn.heatmap(df_cm, annot=True)
    
    if cm_path is not None:
        plt.savefig(cm_path)
        
    # Obtenemos valores de TP, FN, FP y TN y las metricas
    FP = cm.sum(axis=0) - np.diag(cm)
    FN = cm.sum(axis=1) - np.diag(cm)
    TP = np.diag(cm)
    TN = cm.sum() - (FP + FN + TP)

    FP = FP.astype(float)
    FN = FN.astype(float)
    TP = TP.astype(float)
    TN = TN.astype(float)

    # https://towardsdatascience.com/multi-class-classification-extracting-performance-metrics-from-the-confusion-matrix-b379b427a872
    # Sensitivity, hit rate, recall, or true positive rate
    recall = TP/(TP+FN)
    # Specificity or true negative rate
    specificity = TN/(TN+FP) 
    # Precision or positive predictive value
    prec = TP/(TP+FP)
    # Negative predictive value
    NPV = TN/(TN+FN)
    # Fall out or false positive rate
    FPR = FP/(FP+TN)
    # False negative rate
    FNR = FN/(TP+FN)
    # False discovery rate
    FDR = FP/(TP+FP)
    # Overall accuracy for each class
    acc = (TP+TN)/(TP+FP+FN+TN)
    # f1-scores
    f1 = 2*(prec * recall)/(prec + recall)
    
    # Obtenems las métricas por clase
    metrics = {}
    for i, _class in enumerate(classes):
        metrics[_class] = [recall[i], prec[i], f1[i], acc[i]]
    
    return metrics
    
def test_cnn(model, data, cm_path, normalize=False):
    """Función para realizar el test de un modelo de CNN dado el modelo y los datos.
    
    Args:
        model [Keras model]. Modelo entrenado sobre el que proboar las imgs.
        data [TF.Data]. Conjunto de datos cargado con la librería de TF.
        cm_path [String]. Path donde guardar la matriz de confusión
        normalize [Boolean]. Valor para saber si es preciso normalizar los datos de test.
        
    Returns:
        metrics. Métricas por clase del modelo testeado con el conjunto de test.
    
    """
    # Recorremos el conjunto de test y anotamos las predicciones y los Ground Truth
    y_true = []
    y_pred = []
    
    # Normalizamos si es necesario
    if normalize:
        data.test = data.test.map(normalize)
        data.test = data.test.map(_fixup_shape)

    # Recorremos el conjunto de test y sacamos las predicciones
    for i, (im, label) in enumerate(data.test):
            # pred
            pred = model.model.predict(tf.expand_dims(im, axis = 0))
            y_pred.append(np.argmax(pred[0]))
            # gt
            class_index = np.argmax(label.numpy())
            y_true.append(class_index)


    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    
    return get_metrics(y_true, y_pred, data.class_names, cm_path)

def test_fsl(model, feature_extractor_model, query_classes, query_files_path, cm_path, normalize=False):
    """Función para realizar un test completo del modelo de FSL
       
       Args:
           model: [NearestNeighbor]. Modelo de clasificación NearestNeighbor 
           feature_extractor_model: [Keras Model]. Modelo keras CNN para la extracción de caract.
           query_classes: [List]. Listado de clases sobre las que se realiza la prueba
           query_files_path: [List]. Listado de paths de imgs para la prueba
           cm_path [String]. Path donde guardar la matriz de confusión generada
           normalize [Boolean]. Valor para indicar si hay que normalizar o no el valor de entrada al
                                extractor de características
           
       Returns:
           metrics: [Dict]. Clave el nombre de la clase, valor el listado con
                            las métricas por clase [recall, precision, f1, accuracy]
    """
    # Recorremos el conjunto de test y anotamos las predicciones y los Ground Truth
    y_true = []
    y_pred = []

    # Recorremos las imágenes del conjunto Query (Q) y guardamos el vector para cada una de ellas
    dict_query_features = {}
    for i, _class in enumerate(query_classes):
        for j, image_path in enumerate(query_files_path[i]):
            if os.path.isfile(image_path):# para evitar errores con el directorio @eaDir de Synology
                # leemos la imagen
                if normalize:
                    img = plt.imread(image_path).astype('uint8') * (1./255.)
                else:
                    img = plt.imread(image_path).astype('uint8')
                img = tf.image.resize_with_pad(img, target_height=400, target_width=400)

                # para cada una de ellas obtenemos el vector de características
                feature_output = feature_extractor_model(tf.expand_dims(img, axis=0))
                #print(feature_output.numpy()[0])
                # lo incluimos en el listado de la clase
                if _class not in dict_query_features:
                    dict_query_features[_class] = feature_output.numpy()#[0]
                else:
                    dict_query_features[_class] = np.vstack(
                                                    (dict_query_features[_class],
                                                    feature_output.numpy()))#[0]))

    for i, _class in enumerate(query_classes):
        # recorremos los vectores de características de una clase para calcular su predicción
        class_embeddings = dict_query_features[_class]

        #print("{} - {} embeddings".format(_class, class_embeddings.shape))
        for embedding in class_embeddings:
            prediction_class = model.predict([embedding])[0]
            y_true.append(_class)
            y_pred.append(prediction_class)

    y_true = np.array(y_true)
    y_pred = np.array(y_pred)

    return get_metrics(y_true, y_pred, query_classes, cm_path)

def train_model(config_file, model_ft):
    """Función que realiza el entrenamiento del modelo de CNN en 
    base las configuraciones definidas en el fichero pasado por parámetro
    
    Args:
        config_file [String]: Path del fichero de configuración donde se define
                              la configuración del entrenamiento del modelo.
        model_ft [Keras.Model]: Modelo entrenado para FT, utilizado como base para
                                los reentrenamientos.
    Returns:
        [float] Tiempo que ha durado el entrenamiento
        [tf.keras.Model] Modelo extractor de características
        [Config] Clase propia del proyecto en el que encaja el TFE 
                 con las configuraciones de entrenamiento cargadas
        [DataLoader] Clase propia del proyecto en el que encaja el TFE
                     para cargar los datos de test, train y validation
                     según la división indicada en el fichero de config.
        Ejemplo Carga de datos:
            raw_dataset = tf.data.Dataset.list_files(str(self.data_dir/'*/*'), shuffle=False)
            train_ds = self.raw_dataset.take(int(train_size))
            val_ds = self.raw_dataset.skip(train_size).take(val_size)
            test_ds = self.raw_dataset.skip(train_size + val_size).take(test_size)
        
        Ejemplo Carga del modelo CNN solo para reentrenar las capas fully-connected:

            from tensorflow.keras import layers
            # Congelamos los pesos de la capas convolucionales
            model.trainable = False

            # Rehacemos las capas fully connected
            outputs = layers.GlobalAveragePooling2D(name="avg_pool")(model.layers[-5].output)
            outputs = layers.BatchNormalization()(outputs)

            outputs = layers.Dropout(self.config.dropout,
                                    name="top_dropout")(outputs)
            outputs = layers.Dense(
                self.config.data_loader["n_classes"],
                activation="softmax",
                name="classes_prediction")(outputs)

            print("num_classes: {}".format(self.config.data_loader["n_classes"]))

            # Construimos el modelo con Keras Build
            model = tf.keras.Model(model.input, outputs, name="DenseNet121_Ships")

            # Compile model
            model.compile(loss=self.loss,
                        optimizer=self.optimizer,
                        metrics=['accuracy'])
    
    """
    # Cargamos configuración para el entrenamiento del clasificador x-shot_y-novel_z-extra
    config = Config(config_file)
    # cargamos los datos de entrenamiento
    data = factory.DataLoader(config)
    
    # modificamos el número de elementos de train
    ###config.trainer['train_set'] = len(data.train)
    # modificamos el número de elementos de val
    ###config.trainer['val_set'] = len(data.val)
    # modificamos el número de clases del entrenamiento
    config.data_loader['num_classes'] = len(data.class_names)
    config.data_loader['n_classes'] = len(data.class_names)
    
    # Aplicamos data augmentation (albumentation)
    transformer = factory.Transformer(config) #
    data.train = transformer.transform(data.train)
    
    # Preparamos el modelo, esta llamada congelará las capas convolucionales y añadirá capas
    # de fully connected con tantas clases de salida como se requeiran.
    model = factory.Model_TFM_MNM(config, model_ft.model)
    #model.load(config_ft.callbacks['checkpoint_dir'])

    # Eliminamos los entrenamientos previos
    mode_backup_path = "/seda/DL/notebooks/results/experiments/checkpoints/BoatClassif_CU-02_xs_yn_ze/DenseNet121_boats"
    if os.path.exists(mode_backup_path): shutil.rmtree(mode_backup_path)
    mode_backup_path = "/seda/DL/notebooks/results/experiments/checkpoints/BoatClassif_CU-02_xs_yn_ze/EfficientNetB5_boats"
    if os.path.exists(mode_backup_path): shutil.rmtree(mode_backup_path)

    # Preparamos el callback para reducir el LR cada 25 epochs
    custom_callbacks = [tf.keras.callbacks.LearningRateScheduler(step_decay)]

    trainer = factory.Trainer(model.model, data.train, data.val, config,
                            custom_callbacks=custom_callbacks)

    # Entrenamos el modelo
    time_new_1s_1n_start = time.time()
    trainer.train()
    time_new_1s_1n_end = time.time()
    trainning_duration = time_new_1s_1n_end-time_new_1s_1n_start

    print("El entrenamiento con 50 epochs ha durado: {} s".format(trainning_duration))

    return trainning_duration, model, config, data

def train_model_fsl(model_ft, support_classes, support_files_path, normalize=False):
    """Función que realiza el entrenamiento del modelo de FSL en 
    base a las imágenes alojadas en el path que se pasa por parámetro.
    
    Args:
        model_ft [TF Model]: Modelo preentrenado con clases de buques base
        support_classes [List[String]]: Listado de clases nuevas para entrenar 
                                        modelo de FSL.
        support_files_path [List[list[String]]: Listado de lista de imgs de soporte para entrenar
                                          el modelo de FSL.
                                          [["/path/img/class1/1.png", "/path/img/class1/2.png"],
                                           ["/path/img/class2/1.png", "/path/img/class2/2.png"]]
    Returns:
        [float] Tiempo que ha durado el entrenamiento
        [NearestCentroid] Clasificador FSL
        [tf.keras.Model] Modelo extractor de características
        [Dictionary] Diccionario traductor de índice a clase       
    
    """
    model = model_ft.model

    # Nos quedamos solamente con las capas CNN
    layer_name = 'avg_pool'
    feature_extractor_model = tf.keras.Model(inputs=model.input,
                                             outputs=model.get_layer(layer_name).output)

    # Recorremos las imágenes del conjunto Support (S) y guardamos el vector para cada una de ellas
    dict_support_features = {}
    trainning_start_time = time.time()
    for i, _class in enumerate(support_classes):
        for j, image_path in enumerate(support_files_path[i]):
            # leemos la imagen
            if normalize:
                img = plt.imread(image_path).astype('uint8')  * (1./255.)
            else:
                img = plt.imread(image_path).astype('uint8')
            img = tf.image.resize_with_pad(img, target_height=400, target_width=400)

            # para cada una de ellas obtenemos el vector de características
            feature_output = feature_extractor_model(tf.expand_dims(img, axis=0))
            #print(feature_output.numpy()[0])
            # lo incluimos en el listado de la clase
            if _class not in dict_support_features:
                dict_support_features[_class] = feature_output.numpy()#[0]
            else:
                dict_support_features[_class] = np.vstack(
                                                (dict_support_features[_class],
                                                feature_output.numpy()))#[0]))

    # Array que contendrá los puntos centrales obtenidos mediante Nearest Neighbor
    prototype_representation = np.array([])

    # Preparamos los datos para generar el clasificador de Nearest Neighbors
    X = np.array([])
    Y = np.array([])
    index_to_class = {}
    for i, _class in enumerate(dict_support_features):
        if X.shape[0] == 0:
            # primera iteración
            X = dict_support_features[_class]
        else:
            # en el resto concatenamos los resultados
            X = np.concatenate((X, dict_support_features[_class]))
        Y = np.concatenate((Y, np.array([_class]*dict_support_features[_class].shape[0])))

        # Construimos diccionario traductor de index a clase
        index_to_class[i] = _class

    # Obtenemos Prototype representation con los vectores de características de cada clase
    clf = NearestCentroid(metric='euclidean')
    clf.fit(X, Y)

    trainning_end_time = time.time()

    trainning_duration = trainning_end_time - trainning_start_time
    return trainning_duration, clf, feature_extractor_model, index_to_class

def prepare_data(num_train_imgs, num_novel_classes, num_extra_classes):
    """Función para preparar los datos para el entrenamietno del modelo de CNN.
       Repartirá 1 o 5 imágenes, según se indique en "num_train_imgs" y elegirá
       además de las clases base, 1-5 o 1-2 de las clases novel y extra respectivamente
       según lo indicado en "num_novel_classes", "num_extra_classes"
       
       Args:
           num_train_imgs [Int]. Número de imágenes de referencia que poner en train
           num_novel_classes [Int]. Número de clases del grupo novel a elegir para el entrenamiento
           num_extra_classes [Int]. Número de clases del grupo extra a elegir para el entrenamiento
           
       Returns:
           metric_classes [List]. Listado de clases utilizadas para el entrenamiento
    
    """
    trainning_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests"
    fsl_base_classes_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/base/FSL/allImages"
    fsl_new_classes_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/novel/FSL/allImages"
    fsl_extra_classes_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/extra/FSL/allImages"

    paths_fsl = [fsl_base_classes_path, fsl_new_classes_path, fsl_extra_classes_path]


    # limpio el path de entrenamiento
    if os.path.exists(trainning_path): shutil.rmtree(trainning_path)
    #print("BORRADO: {}".format(test_path))
    if not os.path.exists(trainning_path): os.makedirs(trainning_path)

    metric_classes = []
    
    
    for path in paths_fsl:
        # Seteamos la semilla de random según la marca de tiemo, para que en cada pasada sea diferente la eleccion
        random.seed(int(time.time()))

        # Obtener el numero de clases aleatorias
        classes = os.listdir(path)

        if "novel" in path:
            random.shuffle(classes)
            classes =  classes[0:num_novel_classes]
        elif "extra" in path:
            random.shuffle(classes)
            classes =  classes[0:num_extra_classes]


        metric_classes.append(classes)
        #para cada clase muevo 'num_train_imgs' al path 'train' y del resto dividio un 50% validación y 50% para test
        for _class in classes:
            # creamos los paths de train, val y test para el entrenamiento CNN
            dst_path_train = os.path.join(trainning_path, 'train', _class)
            if not os.path.exists(dst_path_train): os.makedirs(dst_path_train)
            dst_path_val = os.path.join(trainning_path, 'val', _class)
            if not os.path.exists(dst_path_val): os.makedirs(dst_path_val)
            dst_path_test = os.path.join(trainning_path, 'test', _class)
            if not os.path.exists(dst_path_test): os.makedirs(dst_path_test)

            # Selecciono
            #print("\tCopio los ficheros para {}".format(_class))
            imgs_class_path = os.path.join(path, _class)
            files = os.listdir(imgs_class_path)
            random.shuffle(files)
            ### TRAIN IMGS
            select_imgs =  files[0:num_train_imgs]
            del files[:num_train_imgs]
            #print("Imagenes elegidas para 'train': {}".format(select_imgs))
            for img in select_imgs:
                #print("\t\t\t[train] COPIADO {} a {}".format(os.path.join(imgs_class_path, img), os.path.join(dst_path_train, img)))
                shutil.copyfile(os.path.join(imgs_class_path, img), os.path.join(dst_path_train, img))

            ### VAL IMGS
            num_val_imgs = int(len(files)*0.5)
            select_imgs =  files[0:num_val_imgs]
            del files[:num_val_imgs]
            #print("Imagenes elegidas de {} para 'val': {}".format(_class, len(select_imgs)))
            for img in select_imgs:
                #print("\t\t\t[val] COPIADO {} a {}".format(os.path.join(imgs_class_path, img), os.path.join(dst_path_val, img)))
                shutil.copyfile(os.path.join(imgs_class_path, img), os.path.join(dst_path_val, img))

            ### TEST IMGS
            #print("Imagenes elegidas de {} para 'test': {}".format(_class, len(files)))
            for img in files:
                #print("\t\t\t[test] COPIADO {} a {}".format(os.path.join(imgs_class_path, img), os.path.join(dst_path_test, img)))
                shutil.copyfile(os.path.join(imgs_class_path, img), os.path.join(dst_path_test, img))

            # print("###### Prepare data #######")
            # print("{} - train: {} - val: {} - test: {}\n".format(_class, len(os.listdir(dst_path_train)), len(os.listdir(dst_path_val)), len(os.listdir(dst_path_test))))
    

    return metric_classes

def prepare_data_to_fsl(num_train_imgs, num_novel_classes, num_extra_classes):
    """Función para preparar los datos para el entrenamietno del modelo de FSL.
       Repartirá 1 o 5 imágenes, según se indique en "num_train_imgs" y elegirá
       además de las clases base, 1-5 o 1-2 de las clases novel y extra respectivamente
       según lo indicado en "num_novel_classes", "num_extra_classes"
       
       Args:
           num_train_imgs [Int]. Número de imágenes de referencia que poner en train
           num_novel_classes [Int]. Número de clases del grupo novel a elegir para el entrenamiento
           num_extra_classes [Int]. Número de clases del grupo extra a elegir para el entrenamiento
           
       Returns:
           metric_classes [List]. Listado de clases utilizadas para el entrenamiento
    
    """
    trainning_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests"
    fsl_base_classes_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/base/FSL/allImages"
    fsl_new_classes_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/novel/FSL/allImages"
    fsl_extra_classes_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/extra/FSL/allImages"

    paths_fsl = [fsl_base_classes_path, fsl_new_classes_path, fsl_extra_classes_path]

    # limpio el path de entrenamiento
    if os.path.exists(trainning_path): shutil.rmtree(trainning_path)
    #print("BORRADO: {}".format(test_path))
    if not os.path.exists(trainning_path): os.makedirs(trainning_path)

    metric_classes = []

    for path in paths_fsl:
        #print("Path origen: {}".format(path))
        random.seed(int(time.time()))

        classes = os.listdir(path)

        if "novel" in path:
            random.shuffle(classes)
            ### TRAIN IMGS
            classes =  classes[0:num_novel_classes]
        elif "extra" in path:
            random.shuffle(classes)
            ### TRAIN IMGS
            classes =  classes[0:num_extra_classes]


        metric_classes.append(classes)
        #para cada clase muevo 'num_train_imgs' al path 'train' y el resto para test
        for _class in classes:
            #print("\tGenero paths para {}".format(_class))
            dst_path_support = os.path.join(trainning_path, 'S', _class)
            if not os.path.exists(dst_path_support): os.makedirs(dst_path_support)
            dst_path_query = os.path.join(trainning_path, 'Q', _class)
            if not os.path.exists(dst_path_query): os.makedirs(dst_path_query)

            #print("\tCopio los ficheros para {}".format(_class))
            imgs_class_path = os.path.join(path, _class)
            files = os.listdir(imgs_class_path)
            random.shuffle(files)
            ### SUPPORT (S) IMGS
            select_imgs =  files[0:num_train_imgs]
            del files[:num_train_imgs]
            #print("Imagenes elegidas para 'train': {}".format(select_imgs))
            for img in select_imgs:
                #print("\t\t\t[S] COPIADO {} a {}".format(os.path.join(imgs_class_path, img), os.path.join(dst_path_support, img)))
                shutil.copyfile(os.path.join(imgs_class_path, img), os.path.join(dst_path_support, img))

            ### QUERY (Q) IMGS
            for img in files:
                #print("\t\t\t[Q] COPIADO {} a {}".format(os.path.join(imgs_class_path, img), os.path.join(dst_path_query, img)))
                shutil.copyfile(os.path.join(imgs_class_path, img), os.path.join(dst_path_query, img))
    

    return metric_classes

def plot_samples(numRows, numCols, classes, images_path, figsize):
    """Función para dibujar ejemplos de las clases a representar

    Args:
        numRows [Int]: Número de filas de la figura a representar
        numCols [Int]: Número de columnas de la figura a representar
        classes [List[Str]]: Listado de clases. Coincidentes con el nombre de
                             carpeta para extrar las imágenes de ejemplo
        imgs_path [Str]: Path de donde extraer las imágenes
        figsize [Tuple(int,int)]: Size de la figura donde representar las imgs

    """
    figure, axes = plt.subplots(nrows=numRows, ncols=numCols, figsize=figsize)
    for i, _class in enumerate(classes):
        class_path = os.path.join(images_path, _class)
        vessel_sample = random.choice(os.listdir(class_path))
        
        array = plt.imread(os.path.join(class_path, vessel_sample))
        
        # Dibujamos en el subplot la imagen y el recuadro del barco indicado
        column = int(i/numRows)
        row = i%numRows
        if len(axes.shape) > 1:
            # Incluimos imagen
            axes[row][column].imshow(array)
            axes[row][column].set_title(_class)
        else:
            # Incluimos imagen
            axes[column].imshow(array)
            axes[column].set_title(_class)


    figure.tight_layout()
    plt.show()
    
def plot_imgs(numRows, numCols, images_path, figsize):
    """Función para dibujar ejemplos de las clases a representar

    Args:
        numRows [Int]: Número de filas de la figura a representar
        numCols [Int]: Número de columnas de la figura a representar
        imgs_path [Str]: Path de donde extraer las imágenes
        figsize [Tuple(int,int)]: Size de la figura donde representar las imgs

    """
    figure, axes = plt.subplots(nrows=numRows, ncols=numCols, figsize=figsize)
    for i, img_path in enumerate(images_path):
        array = plt.imread(img_path)
        
        # Dibujamos en el subplot la imagen y el recuadro del barco indicado
        column = int(i/numRows)
        row = i%numRows
        if len(axes.shape) > 1:
            # Incluimos imagen
            axes[row][column].imshow(array)
        else:
            # Incluimos imagen
            axes[row].imshow(array)


    figure.tight_layout()
    plt.show()
    
def add_value_labels(ax, spacing=5):
    """Add labels to the end of each bar in a bar chart.

    Arguments:
        ax (matplotlib.axes.Axes): The matplotlib object containing the axes
            of the plot to annotate.
        spacing (int): The distance between the labels and the bars.
    """

    # For each bar: Place a label
    for rect in ax.patches:
        # Get X and Y placement of label from rect.
        y_value = rect.get_height()
        x_value = rect.get_x() + rect.get_width() / 2

        # Number of points between bar and label. Change to your liking.
        space = spacing
        # Vertical alignment for positive values
        va = 'bottom'

        # If value of bar is negative: Place label below bar
        if y_value < 0:
            # Invert space to place label below
            space *= -1
            # Vertically align label at top
            va = 'top'

        # Use Y value as label and format number with one decimal place
        label = "{:.2f}".format(y_value)

        # Create annotation
        ax.annotate(
            label,                      # Use `label` as label
            (x_value, y_value),         # Place label at end of the bar
            xytext=(0, space),          # Vertically shift label by `space`
            textcoords="offset points", # Interpret `xytext` as offset in points
            ha='center',                # Horizontally center label
            va=va)                      # Vertically align label differently for
                                        # positive and negative values.

def crop_and_save(df, origin_images_path, dest_images_path):
    """
    Función para recortar y guardar los recortes en la carpeta correspondiente para el entrenamiento y test de los modelos.

        df: Pandas DataFrame
        origin_images_path: String, path donde se encuentran las imágenes
        dest_images_path: String, path donde guardar los recortes

    """
    # Recorremos imágenes para ir haciendo recortes de cada barco que aparece en ella y guardar en la carpeta correspondiente
    crops = []
    counter=0
    for filename in df.filename.unique(): # recorrer los ficheros
        img_array = plt.imread(os.path.join(origin_images_path, filename))
        #plt.imshow(img_array)
        
        df_ = df[df['filename']==filename]
        for _, row in df_.iterrows():# Recorrer todos los barcos de cada fichero
            x = row['x_min']
            y = row['y_min']
            side_x = row['width']
            side_y = row['height']
            
            cx = row['mbox_cx']
            cy = row['mbox_cy']
            l = row['mbox_w']
            b = row['mbox_h']
            angle = row['mbox_ang']
            degree = np.degrees(row['mbox_ang'])
            set_folder = row['set']
            folder = row['save_in']
            corner_1_x, corner_1_y = (cx+l/2*np.cos(angle)+b/2*np.cos(np.pi/2-angle), cy+l/2*np.sin(angle)-b/2*np.sin(np.pi/2-angle))
            corner_2_x, corner_2_y = (cx+l/2*np.cos(angle)-b/2*np.cos(np.pi/2-angle), cy+l/2*np.sin(angle)+b/2*np.sin(np.pi/2-angle))
            corner_3_x, corner_3_y = (cx-l/2*np.cos(angle)-b/2*np.cos(np.pi/2-angle), cy-l/2*np.sin(angle)+b/2*np.sin(np.pi/2-angle))
            corner_4_x, corner_4_y = (cx-l/2*np.cos(angle)+b/2*np.cos(np.pi/2-angle), cy-l/2*np.sin(angle)-b/2*np.sin(np.pi/2-angle))
            
            x_min = min(corner_1_x,corner_2_x,corner_3_x,corner_4_x)
            y_min = min(corner_1_y,corner_2_y,corner_3_y,corner_4_y)
            x_max = max(corner_1_x,corner_2_x,corner_3_x,corner_4_x)
            y_max = max(corner_1_y,corner_2_y,corner_3_y,corner_4_y)
            
            pts = np.array([[int(corner_1_x), int(corner_1_y)], [int(corner_2_x), int(corner_2_y)], [int(corner_3_x), int(corner_3_y)], [int(corner_4_x), int(corner_4_y)]])
            ### (1) Crop the bounding rect
            rect = cv2.boundingRect(pts)
            x,y,w,h = rect
            x = int(np.clip(x, 0, np.inf))
            y = int(np.clip(y, 0, np.inf))
            cropped = img_array[y:y+h, x:x+w].copy()

            ## (2) make mask
            pts = pts - pts.min(axis=0)

            mask = np.zeros(cropped.shape[:2], np.uint8)
            cv2.drawContours(mask, [pts], -1, (255, 255, 255), -1, cv2.LINE_AA)

            ## (3) do bit-op
            dst = cv2.bitwise_and(cropped, cropped, mask=mask)
            
            rotated_array = ndimage.rotate(dst, degree)
            shape_rotated = rotated_array.shape

            crop_rotated = rotated_array[int(shape_rotated[0]/2-b/2):int(shape_rotated[0]/2-b/2+b),int(shape_rotated[1]/2-l/2):int(shape_rotated[1]/2-l/2+l),:]
            
            name_image = str(counter)+'.tif'
            if not os.path.exists(os.path.join(dest_images_path, set_folder, folder)):
                os.makedirs(os.path.join(dest_images_path, set_folder, folder))
            imageio.imwrite(os.path.join(dest_images_path, set_folder, folder, name_image), crop_rotated)
            counter +=1
            
def toFloat32(img, lbl):
    img = tf.cast(img, tf.float32)
    img *= 1. / 255.
    lbl = tf.cast(lbl, tf.uint8)
    return img, lbl

def normalize(img, lbl):
    data = tf.numpy_function(func=toFloat32,
                            inp=[img, lbl],
                            Tout=[tf.float32, tf.uint8])
    return data


def _fixup_shape(images, lbl):
    images.set_shape([None, None, None])
    lbl.set_shape([None, ])
    return images, lbl

def step_decay(epoch):
    """
    Reduce a la mitad el lr cada 'epochs_drop'
    ref1: https://www.tensorflow.org/api_docs/python/tf/keras/callbacks/LearningRateScheduler
    ref2: https://towardsdatascience.com/learning-rate-schedules-and-adaptive-learning-rate-methods-for-deep-learning-2c8f433990d1
    """
    initial_lrate = 0.0001
    drop = 0.5
    epochs_drop = 10.0#25.0
    lrate = initial_lrate * math.pow(drop,  
            math.floor((1+epoch)/epochs_drop))
    return lrate


def main(args):
    # Número de repeticiones 
    REPEATS = args.repeats

    # extra_imgs_path, novel_imgs_path, base_imgs_path
    results_path = '/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/results'
    config_FT_file_path = 'configs/seda_boat_classif_CU-02_FT_efficientnet.json'
    config_file_path = 'configs/seda_boat_classif_CU-02_x-shot_y-novel_y-extra_efficientnet.json'
    # config_FT_file_path = 'configs/seda_boat_classif_CU-02_FT.json' #Densenet121
    # config_file_path = 'configs/seda_boat_classif_CU-02_x-shot_y-novel_y-extra.json'#DenseNet121

    support_files_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests/S"
    query_files_path = "/var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests/Q"

    # Cargamos el modelo preentrenado con FT para el entrenamiento de FSL
    config_ft = Config(config_FT_file_path)
    model_ft = factory.Model(config_ft)
    model_ft.load(config_ft.callbacks['checkpoint_dir'])

    num_imgs_shot_novel = [1, 5, 1, 5]
    num_class_shot_novel = [1, 1, 5, 5]
    num_class_shot_extra = [1, 1, 2, 2]

    training_sets = ["1-shot_1-novel_1-extra", "5-shot_1-novel_1-extra", "1-shot_5-novel_2-extra", "5-shot_5-novel_2-extra"]

    # ejecución del bucle
    for i, _set in enumerate(training_sets):
        

        report_data_cnn = []
        report_data_fsl = []
        for num_experiment in range(REPEATS):
            ################  CNN  ###############
            print("#####> Training CNN {}".format(_set))
            time_stamp_cnn = time.strftime("%Y%m%d-%H%M%S")
            print("\t---------------- [{}] ---------------".format(num_experiment))
            report_path_cnn = os.path.join(results_path, "reports", "cnn", _set +"_"+ time_stamp_cnn+".csv")
            
            # Separacion de imagenes en train, test y validación de manera aleatoria
            metric_classes = prepare_data(num_imgs_shot_novel[i], num_class_shot_novel[i], num_class_shot_extra[i])        
            # Entrenar modelo
            trainning_duration, model, config, data = train_model(config_file_path, model_ft)
            # Obtencion de metricas
            if not os.path.exists(os.path.join(results_path, 'imgs', 'cnn', _set)): os.makedirs(os.path.join(results_path, 'imgs', 'cnn', _set)) 
            cm_path = os.path.join(results_path, 'imgs', 'cnn', _set, time_stamp_cnn+"_"+str(num_experiment)+".png")
            metrics = test_cnn(model, data, cm_path, normalize=config.trainer['normalized_img'])
            # Incluimos metricas en el listado del informe
            report_data_cnn.append([metric_classes, trainning_duration, metrics, cm_path])

            # creo el fichero de informe csv con los datos almacenados
            df_report_cnn = pd.DataFrame(report_data_cnn, 
                                    columns=['classes', 'trainning_duration', 'metrics_per_class', 'confusion_matrix_path'])
            df_report_cnn.to_csv(report_path_cnn, index=False)


            ################  FSL  ###############
            print("#####> Training FSL {}".format(_set))
            time_stamp_fsl = time.strftime("%Y%m%d-%H%M%S")
            print("\t---------------- [{}] ---------------".format(num_experiment))
            report_path_fsl = os.path.join(results_path, "reports", "fsl", _set +"_"+ time_stamp_fsl+".csv")

            # Ajuste de paths. Los preparamos para FSL
            # Movemos las imgs de validation a test
            os.system('cp -r /var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests/val/* /var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests/test')
            # Borramos la carpeta de validation
            os.system('rm -r /var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests/val')
            # Renombramos las carpetas
            os.system('mv /var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests/test /var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests/Q')
            os.system('mv /var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests/train /var/seda-datasets/1.DS_Terceros/1.1.RGB/HRSC2016/HRSC2016/CU-02/training_tests/S')

            # Entrenar modelo
            support_classes = os.listdir(support_files_path)
            support_files = []
            for _class in support_classes:
                class_path = os.path.join(support_files_path, _class)
                support_files.append([os.path.join(class_path, i) for i in os.listdir(class_path)])
            trainning_duration, model_fsl, feature_extractor_model, index_to_class = train_model_fsl(model_ft, support_classes, support_files, normalize=config_ft.trainer['normalized_img'])
                
            # Obtencion de metricas
            if not os.path.exists(os.path.join(results_path, 'imgs', 'fsl', _set)): os.makedirs(os.path.join(results_path, 'imgs', 'fsl', _set)) 
            cm_path = os.path.join(results_path, 'imgs', 'fsl', _set, time_stamp_fsl+"_"+str(num_experiment)+".png")

            query_classes = sorted(os.listdir(query_files_path))
            query_files = []
            for _class in query_classes:
                class_path = os.path.join(query_files_path, _class)
                list_files = [os.path.join(class_path, i) for i in os.listdir(class_path)]
                query_files.append(list_files)

            metrics = test_fsl(model_fsl, feature_extractor_model, query_classes, query_files, cm_path, normalize=config_ft.trainer['normalized_img'])

            # Incluimos metricas en el listado del informe
            report_data_fsl.append([metric_classes, trainning_duration, metrics, cm_path])

            # creo el fichero de informe csv con los datos almacenados
            df_report_fsl = pd.DataFrame(report_data_fsl, 
                                    columns=['classes', 'trainning_duration', 'metrics_per_class', 'confusion_matrix_path'])
            df_report_fsl.to_csv(report_path_fsl, index=False)



if __name__ == '__main__':
    physical_devices = tf.config.list_physical_devices('GPU') 
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

    # Parse command arguments
    p = argparse.ArgumentParser(description=__doc__)
    p.add_argument("--repeats", type=int, default=1)
    args = p.parse_args()

    main(args)
